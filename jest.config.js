module.exports = {
  setupFiles: ['./tests/setup/setEnvironment.js'],
  // preset: '@shelf/jest-dynamodb',
  transform: {
    '^.+\\.ts?$': 'babel-jest',
    // moduleNameMapper: {
    //   '^handler': '<rootDir>/src/handler',
    // },
  },
};
