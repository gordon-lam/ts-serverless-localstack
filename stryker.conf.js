module.exports = (config) => {
  config.set({
    mutate: ['!src/tests/**/*.test.ts', 'src/**/*.ts'],
    mutator: 'typescript',
    packageManager: 'npm',
    reporters: ['clear-text', 'progress'],
    testRunner: 'jest',
    transpilers: ['babel'],
    coverageAnalysis: 'off',
    babel: {
      optionsFile: 'babel.config.js',
    },
  });
};
