

Running Localstack
There are multiple ways to run Localstack.

Starting Localstack via Docker
If Localstack is installed via pip

localstack start --docker
If Localstack is installed via source

make docker-run