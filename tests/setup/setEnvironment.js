const env = {
  DYNAMO_TABLE: 'SAWaterData',
  AWS_REGION: 'local-env',
  AWS_ACCESS_KEY: 'fake_key',
  AWS_SECRET_KEY: 'fake_secret',
  FLEET_INFLUX_DB_USERNAME: 'fleet',
  FLEET_INFLUX_DB_PASSWORD: 'connecteverything',
};

process.env = {
  ...process.env,
  ...env,
};
