type TResult = {
  statusCode: number;
  body: string;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const helloHandler = async (event): Promise<TResult> => {
  const body: object = { result: 'result' };

  return {
    statusCode: 200,
    body: JSON.stringify(body),
  };
};

// eslint-disable-next-line import/prefer-default-export
export { helloHandler };
