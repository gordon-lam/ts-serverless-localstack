// eslint-disable-next-line import/no-extraneous-dependencies
import createEvent from 'aws-event-mocks';
import { helloHandler } from './handler';

describe('api Get function handler', () => {
  it('that handler', async () => {
    // Setup
    expect.hasAssertions();

    const apiEvent = createEvent({
      template: 'aws:apiGateway',
    });

    const result = await helloHandler(apiEvent);
    const expectedResult = {
      statusCode: 200,
      body: JSON.stringify({ result: 'result' }),
    };

    expect(result).toStrictEqual(expectedResult);
  });
});
